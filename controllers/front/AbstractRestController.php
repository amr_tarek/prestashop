<?php

abstract class AbstractRestController extends ModuleFrontController
{
    public function init() 
    {
        parent::init();
        switch ($_SERVER['REQUEST_METHOD']) {
           
            case 'POST':
                $this->processPostRequest();
                break;
            default:
                // throw some error or whatever
        }
    }

    
    abstract protected function processPostRequest();
    
    
}