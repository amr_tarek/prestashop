<?php

require_once(dirname(__FILE__).'/AcceptOrderTrait.php');

class AcceptValuModuleFrontController extends ModuleFrontController
{   
    use AcceptOrderTrait;

    public function initContent()
    {
        parent::initContent();

        if(Tools::Isset('hmac') && Tools::getvalue('hmac')){

            if(($_SERVER['REQUEST_METHOD'] === 'POST')){
                return $this->postProcess();
            }
            if(($_SERVER['REQUEST_METHOD'] === 'GET')){
                return $this->redirectUrl();
            }
        }
        //die(dump($this->context->link->getModuleLink('jawwalpay', 'iframe', array(), true)));
        
        // //check for redirect url from accept
        // if ($this->isResponseCallback()) 
        // return $this->redirectUrl();

        // if ($this->isResponseCallback()) 
        // return $this->postProcess();
        
        $proceed = true;
        
        /*
         * If the module is not active anymore, no need to process anything.
         */
        if ($this->module->active == false) {
            die;
        }

        $authorized = false;
        $cart = $this->context->cart;
        
        // if(Context::getContext()->currency->iso_code != Configuration::get('credit_cards_currency'))
        //  Tools::redirect('index.php?controller=order&step=1');
        if(Context::getContext()->currency->iso_code != Configuration::get('valu_currency')){
            $this->context->cookie->id_cart = (int) $cart->id;
            Tools::redirect('index.php?controller=order&step=1');
        }

          /**
         * Verify if this payment module is authorized
         */
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'accept') {
                $authorized = true;
                break;
            }
        }

        if (!$authorized) {
            die($this->l('This payment method is not available.'));
        }

       
        
        /**
         * Get current cart object from session
         */
       
       
 
        /**
         * Verify if this module is enabled and if the cart has
         * a valid customer, delivery address and invoice address
         */
        if (!$this->module->active || $cart->id_customer == 0 || $cart->id_address_delivery == 0
            || $cart->id_address_invoice == 0) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        //die(dump($cart));



        /** @var CustomerCore $customer */
        $customer = new Customer($cart->id_customer);

        /**
         * Check if this is a vlaid customer account
         */
        if (!Validate::isLoadedObject($customer)) {
            Tools::redirect('index.php?controller=order&step=1');
        }
 
      
        
        // if ($this->isValidOrder() === true) {
        //     $payment_status = Configuration::get('PS_OS_PAYMENT');
        //     $message = null;
        // } else {
        //     $payment_status = Configuration::get('PS_OS_ERROR');

        //     /**
        //      * Add a message to explain why the order has not been validated
        //      */
        //     $message = $this->module->l('An error occurred while processing payment');
        // }

        

        $cart = $this->context->cart;

        $module_name = $this->module->displayName;
        
        $currency_id = (int) Context::getContext()->currency->id;

        $currencyCode = Context::getContext()->currency->iso_code;

        $amount = (float) $cart->getOrderTotal(true, Cart::BOTH);

        $payment_status = Configuration::get('PS_OS_PREPARATION');

        $orderAdded = $this->module->validateOrder(
        (int) $cart->id,
        $payment_status,
        $amount,
        $module_name,
        "pending till getaway confirmation",
        array(),
        $currency_id, 
        false,
        $customer->secure_key);

        if(!$orderAdded) $proceed = false;
        // print_r($amount);
        // die;
        $id_order = Order::getOrderByCartId($cart->id);

        //$order_payment = OrderPayment::getByOrderId($id_order);

        //die(dump($cart->getSummaryDetails(),$customer));

        if($proceed)
        $token = $this->requestToken(Configuration::get('valu_api_key'),
        $this->getCreditCardUrl('/api/auth/tokens'));
        
        if(!$token) $proceed = false;
        $items = array(array("name" => 'Total amount - order #'.$id_order,
                        "amount_cents" => intval($amount)
                    ));
        
        $jawwalOrderId = '';
        if($proceed)
        $jawwalOrderId = $this->createOrder($token,
        true,
        Configuration::get('valu_merchant'),
        $amount,
        $currencyCode,
        $id_order,
        $items,
        [],
        $this->getCreditCardUrl('/api/ecommerce/orders'));

        if(!$jawwalOrderId) $proceed = false;
        
        $billingAndDelivery = $cart->getSummaryDetails();
        
        $delivery = $billingAndDelivery['delivery'];

        $billing = $billingAndDelivery['invoice'];

        $billing_data = $this->parseOrder($delivery,
        $billing,
        $customer->email,
        $billingAndDelivery['delivery_state']);
        
        $paymentReturn = '';
        if($proceed)
        $paymentReturn = $this->createPaymentKey($token,
        $amount,
        $jawwalOrderId,
        $billing_data,
        $currencyCode,
        Configuration::get('valu_int'),
        $this->getCreditCardUrl('/api/acceptance/payment_keys')
        );
        if(isset($paymentReturn['token']) )
        {
        	$iframeKey = $paymentReturn['token'];
        }else{
        	$proceed = false;
        }


        if(!$proceed){
                $this->context->cookie->id_cart = (int) $cart->id;
		$this->context->smarty->assign([
			    'message' => $paymentReturn['error'],
			    'src' => '',
			    'success' => false
		]);
		 if (version_compare(_PS_VERSION_,'1.7.0','>')){
		    $this->setTemplate('module:accept/views/templates/front/acceptRedirect2.tpl');
		}else{
		    $this->setTemplate('acceptRedirect.tpl');
		}
        }else{
		$this->context->smarty->assign(array(
		    'src' => $this->getCreditCardIframeUrl($iframeKey)
		));
		if (version_compare(_PS_VERSION_,'1.7.0','>')){
		    $this->setTemplate('module:accept/views/templates/front/acceptIframe2.tpl');
		}else{
		    $this->setTemplate('acceptIframe.tpl');
		}
	}
	
        		
    }


    
        
    //     if(!$paymentReturn) $proceed = false;

    //     //if(!$proceed)Tools::redirect('index.php?controller=order&step=1');

    //     $this->context->smarty->assign([
    //         'src' => $this->getCreditCardIframeUrl($paymentReturn)
    //     ]);

    //     //$this->setTemplate('module:accept/views/templates/front/creditcardIframe.tpl');
    //     //$this->getCreditCardIframeUrl($paymentReturn);
    //     $this->setTemplate('module:accept/views/templates/front/acceptIframe2.tpl');
    //     // if (version_compare(_PSVERSION,'1.7.0','>')){
    //     //     $this->setTemplate('module:accept/views/templates/front/acceptIframe2.tpl');
    //     // }else{
    //     //     $this->setTemplate('acceptIframe.tpl');
    //     // }
    // if(!$proceed){
    //         $this->context->cookie->id_cart = (int) $cart->id;
    //         $this->context->smarty->assign([
    //         'message' => $paymentReturn['error'],
    //         'src' => '',
    //         'success' => false
    // ]);
    // $this->setTemplate('module:accept/views/templates/front/acceptRedirect2.tpl');
    //  if (version_compare(_PSVERSION,'1.7.0','>')){
    //     $this->setTemplate('module:accept/views/templates/front/acceptRedirect2.tpl');
    // }else{
    //     $this->setTemplate('acceptRedirect.tpl');
    // }
    // }else{
    //     $this->getCreditCardIframeUrl($paymentReturn);
    // // $this->context->smarty->assign(array(
    // //     'src' => $this->getCreditCardIframeUrl($paymentReturn)
    // // ));
    // if (version_compare(_PSVERSION,'1.7.0','>')){
    //     $this->setTemplate('module:accept/views/templates/front/acceptIframe2.tpl');
    // }else{
    //     $this->setTemplate('acceptIframe.tpl');
    // }
    //     }
    
    
    

    public function postProcess(){
       
        if(!($_SERVER['REQUEST_METHOD'] === 'POST'))return false;

        $json = json_decode(file_get_contents('php://input'), true);
        $type = $json['type'];
        $data = $json['obj'];
        $obj  = $json['obj'];
        
        if ($type === 'TRANSACTION')
        {
            $data['order']                  = $data['order']['id'];
            $data['is_3d_secure']           = ($data['is_3d_secure'] === false) ? 'true' : 'false';
            $data['is_auth']                = ($data['is_auth'] === true) ? 'true' : 'false';
            $data['is_capture']             = ($data['is_capture'] === true) ? 'true' : 'false';
            $data['is_refunded']            = ($data['is_refunded'] === true) ? 'true' : 'false';
            $data['is_standalone_payment']  = ($data['is_standalone_payment'] === true) ? 'true' : 'false';
            $data['is_voided']              = ($data['is_voided'] === true) ? 'true' : 'false';
            $data['success']                = ($data['success'] === true) ? 'true' : 'false';
            $data['error_occured']          = ($data['error_occured'] === true) ? 'true' : 'false';
            $data['has_parent_transaction'] = ($data['has_parent_transaction'] === true) ? 'true' : 'false';
            $data['pending']                = ($data['pending'] === true) ? 'true' : 'false';
            $data['source_data_pan']        = $data['source_data']['pan'];
            $data['source_data_type']       = $data['source_data']['type'];
            $data['source_data_sub_type']   = $data['source_data']['sub_type'];
            
            //die(dump( $data));
            PrestaShopLogger::addLog("passed data" , 1);
            // print_r($data);
            // die;
            $hash = $this->calculateHash(Configuration::get('valu_hmac'),$data,$type);
           
            // auth?
            if ($hash === Tools::getValue('hmac')) {
                PrestaShopLogger::addLog("passed hmac" , 1);
                
                $order_id = $obj['order']['merchant_order_id'];
                
                if(!$order_id) die("no order with this id");

                if (
                    $obj['success'] === true &&
                    $obj['is_voided'] === false &&
                    $obj['is_refunded'] === false &&
                    $obj['pending'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false &&
                    $obj['error_occured'] === false
                    
                ) {
                    //Payment Sucessfull
                    PrestaShopLogger::addLog("Payment Sucessfull" , 1);
                    $this->updateOrderStatus($order_id, Configuration::get('PS_OS_PAYMENT'));
                    die("Payment Sucessfull");
                    
                } elseif (
                    $obj['success'] === true &&
                    $obj['is_refunded'] === true &&
                    $obj['is_voided'] === false &&
                    $obj['pending'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false
                ) {
                    $this->updateOrderStatus($order_id, Configuration::get('PS_OS_REFUND'));
                    die("Payment Refunded");

                } elseif (
                    $obj['success'] === true &&
                    $obj['is_voided'] === true &&
                    $obj['is_refunded'] === false &&
                    $obj['pending'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false
                ) {
                    //Payment Voided 
                    $this->updateOrderStatus($order_id, Configuration::get('PS_OS_ORDER_CANCELED'));
                    die("Payment Canceled");
                   
                } elseif (
                    $obj['success'] === false &&
                    $obj['is_voided'] === false &&
                    $obj['is_refunded'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false
                ) {
                    //Payment Pending
                    die("Payment Pending");
                }

                       
            } else {
                die('HMAC Failed to validate.');
            }
    
            die('end of update operation');
        }
    }

    // public function redirectUrl()
    // {   
    //     $message = "sorry, your payment dosn't approved";
    //     $success = false;

    //     if(Tools::getIsset('order') && Tools::getIsset('data_message') && 
    //     Tools::getIsset('success') && Tools::getIsset('id') && 
    //     Tools::getValue('success') && Tools::getValue('data_message') == 'Approved'){
    //         $objOrder = new Order($_GET["merchant_order_id"]); //order with id=$_GET["action"]
    //         $history = new OrderHistory();
    //         $history->id_order = (int)$objOrder->id;
    //         $history->changeIdOrderState(Configuration::get('PS_OS_PAYMENT'), (int)($objOrder->id)); //order status=4
    //         $history->addWithemail();
    //    	    $history->save();
    //         $message = "Your payment has been approved";
    //         $success = true;
    //     }
        
       
    //     $this->context->smarty->assign([
    //         'message' => $this->l($message),
    //         'success' => $success
    //     ]);

    //     //$this->setTemplate('module:accept/views/templates/front/creditcardRedirect.tpl');
    //     $this->setTemplate('module:accept/views/templates/front/acceptRedirect2.tpl');
    //     // if (version_compare(_PSVERSION,'1.7.0','>')){
    //     //     $this->setTemplate('module:accept/views/templates/front/acceptRedirect2.tpl');
    //     // }else{
    //     //     $this->setTemplate('acceptRedirect.tpl');
    //     // }
         
    // }
    public function redirectUrl()
    {   
        $success = false;
        // print_r(Tools::getIsset('order') .' '. Tools::getIsset('data_message') .' '. Tools::getIsset('success') . ' '. Tools::getIsset('id') .' '.
        // Tools::getValue('success') .' '. Tools::getValue('data_message'));
        // die;
        if(Tools::getIsset('order') && Tools::getIsset('data_message') && 
        Tools::getIsset('success') && Tools::getIsset('id') && 
        Tools::getValue('success')){
            $objOrder = new Order($_GET["merchant_order_id"]); //order with id=$_GET["action"]
            $history = new OrderHistory();
            $history->id_order = (int)$objOrder->id;
            $history->changeIdOrderState(Configuration::get('PS_OS_PAYMENT'), (int)($objOrder->id)); //order status=4
            $history->addWithemail();
       	    $history->save();

            $message = "Your payment has been approved approved";
            $success = true;
        }
        
               
       if($success == false){
            $objOrder = new Order($_GET["merchant_order_id"]); //order with id=$_GET["action"]
           $history = new OrderHistory();
            $history->id_order = (int)$objOrder->id;
            $history->changeIdOrderState(Configuration::get('PS_OS_ERROR'), (int)($objOrder->id)); //order status=4
            $history->save();

            $message = "sorry, your payment isn't approved with error ".Tools::getValue('data_message');
       }
       
        $this->context->smarty->assign([
            'message' => $message,
            'success' => $success
        ]);
        

        if (version_compare(_PS_VERSION_,'1.7.0','>')){
            $this->setTemplate('module:accept/views/templates/front/acceptRedirect2.tpl');
        }else{
            $this->setTemplate('acceptRedirect.tpl');
        }

    }

    private function isResponseCallback()
    {
        if(Tools::getIsset('order') &&
            Tools::getIsset('data_message') && 
            Tools::getIsset('success') &&
            Tools::getIsset('id')
        ) return true; 

        return false;
    }
    

    protected function isValidOrder()
    {
        /*
         * Add your checks right there
         */
        return true;
    }

    private function getCreditCardUrl($route)
    {
        return Configuration::get('valu_domain').$route;
    }

    private function getCreditCardIframeUrl($token)
    {
        //foreach($token as $x => $val) {
            //echo "$val<br>";
            //return $val;
            
            return Configuration::get('valu_domain')
            .'/api/acceptance/iframes/'.
            Configuration::get('valu_iframe').
            '?payment_token='.$token;
          
          
        
    }

  


}