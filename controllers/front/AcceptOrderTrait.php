<?php

require_once _PS_CORE_DIR_.'/classes/order/Order.php';
require_once _PS_CORE_DIR_.'/classes/order/OrderHistory.php';

Trait AcceptOrderTrait{
   
    public function updateOrderStatus($id_order,$status)
    {
        $objOrder = new Order($id_order); 
        $history = new OrderHistory();
        $history->id_order = (int)$objOrder->id;
        $history->changeIdOrderState($status, (int)($objOrder->id)); 
    }

    // move to trait 
    /**
     * @param $order
     */
    public function parseOrder($delivery,$billing,$email,$delivery_state)
    {
        $data = [];
        
        $data['shipping_data'] = [
            'first_name'      => $delivery->firstname,
            'last_name'       => $delivery->lastname,
            'email'           => $email,
            'street'          => $delivery->address1. ' - ' . $delivery->address2,
            'phone_number'    => $delivery->phone,
            'city'            => $delivery->city,
            'country'         => $delivery->country,
            'state'           => ($delivery_state)? $billing->postcode : 'NA',
            'postal_code'     => ($delivery->postcode)? $delivery->postcode : 'NA',
            'apartment'       => 'NA',
            'floor'           => 'NA',
            'building'        => 'NA',
            'shipping_method' => 'UNK',
        ];

        $data['billing_data'] = [
            'first_name'      => $billing->firstname,
            'last_name'       => $billing->lastname,
            'email'           => $email,
            'street'          => $billing->address1. ' - ' . $delivery->address2,
            'phone_number'    => $billing->phone,
            'city'            => $billing->city,
            'country'         => $billing->country,
            'state'           => ($delivery_state)? $billing->postcode : 'NA',
            'postal_code'     => ($billing->postcode)? $billing->postcode : 'NA',
            'apartment'       => 'NA',
            'floor'           => 'NA',
            'building'        => 'NA',
            'shipping_method' => 'PKG',
        ];
        //print_r($data);
        //die;
        return $data;
    }

    /**
     * @param  $api_key
     * @return mixed
     */
    public function requestToken($api_key,$url)
    {
        $data = [
            'api_key' => $api_key,
        ];

        $request = $this->HttpPost($url, $data);
        
        if(isset($request['token'])) return $request['token'];

        return false;
    }

    /**
     * @param  $delivery_needed
     * @param  $merchant_id
     * @param  $amount_cents
     * @param  $currency
     * @param  $merchant_order_id
     * @param  $items
     * @param  $shipping_data
     * @return mixed
     */
    public function createOrder($auth, $delivery_needed, $merchant_id, $amount_cents, $currency, $merchant_order_id, $items, $shipping_data,$url)
    {        
        $data = [
            'delivery_needed'   => $delivery_needed,
            'merchant_id'       => $merchant_id,
            'amount_cents'      => $amount_cents*100,
            'currency'          => $currency,
            'merchant_order_id' => $merchant_order_id,
            'items'             => $items,
            //'shipping_data'     => $shipping_data,
        ];

        $order = $this->HttpPost($url, $data, $auth);
      //  print_r( $order); die;
        if(isset($order['id'])) return $order['id'];

        return false;
    }

    /**
     * @param  $order_id
     * @return mixed
     */
    public function createPaymentKey($auth, $amount_cents, $order_id, $billing_data, $currency, $integration_id, $url)
    {
    
    	
        $data = [
            'amount_cents'   => $amount_cents * 100,
            'expiration'     => 36000,
            'order_id'       => $order_id,
            'billing_data'   => $billing_data['billing_data'],
            'shipping_data'  => $billing_data['shipping_data'],
            'currency'       => $currency,
            'integration_id' => $integration_id,
        ];

        $payment = $this->HttpPost($url, $data, $auth);

        if(isset($payment['token'])){ 
        	return $payment;
        }else{
        	$message = null;
        	foreach($payment as $key => $val){
        		$message = $key ." ";
	        	foreach($val as $key2 => $val2){	        		
	        		$message.= $key2 . ' : ' .$val2[0] ."<br>";
	        	}
        	}
		return array('error'=>$message);
        }
    }

    /**
     * @param  $billing
     * @param  $payment_token
     * @return mixed
     */
    public function requestKioskId($auth, $payment_token)
    {
        $data = ['source' => ['identifier' => 'AGGREGATOR', 'subtype' => 'AGGREGATOR'],
            //'billing'         => $billing,
            'payment_token'   => $payment_token,
        ];

        $kiosk = $this->HttpPost('https://accept.paymobsolutions.com/api/acceptance/payments/pay', $data, $auth);
        
        // print_r($kiosk['id']);
        // die;

        return $kiosk['id'];
    }
    public function requestwalleturl($phone_number,$auth,$payment_token)
    {
        $data = [
            "source"        => ["identifier"=> $phone_number, "subtype"=>"WALLET"],
            //"billing"       => $this->data,
            "payment_token" => $payment_token,
        ];
        //print_r($phone_number);
        //die;

        $request = $this->HttpPost("https://accept.paymobsolutions.com/api/acceptance/payments/pay", $data, $auth);
        //print_r($request);
        //die;
        if ($request) {
            if( isset($request['redirect_url']) )
            {
                return $request['redirect_url'];
            }
        }

        return false;
    }

    /**
     * @param  $url_path
     * @param  array       $data
     * @return mixed
     */
    private function HttpPost($url_path, $data = [], $auth = null)
    {
        $ch      = curl_init();
        $target  = $url_path;
        $options = [
            CURLOPT_URL            => $target,
            CURLOPT_HTTPHEADER     => ['Content-Type: application/json', "Authorization: Bearer $auth"],
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER  =>  0,
            CURLOPT_SSL_VERIFYHOST  =>  0,
            CURLOPT_HEADER         => false,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => json_encode($data),
        ];

        curl_setopt_array($ch, $options);
        $response           = curl_exec($ch);
        $response_json_data = json_decode($response, true);
        $errors             = curl_error($ch);
        curl_close($ch);

        if ($response_json_data) {
            return $response_json_data;
        }

        return $errors;
    }

    /**
     * @param  $key
     * @param  $data
     * @param  $type
     * @return mixed
     */
    public static function calculateHash($key, $data, $type)
    {
        $str = '';
        switch ($type) {
            case 'TRANSACTION':
                $str =
                    $data['amount_cents'] .
                    $data['created_at'] .
                    $data['currency'] .
                    $data['error_occured'] .
                    $data['has_parent_transaction'] .
                    $data['id'] .
                    $data['integration_id'] .
                    $data['is_3d_secure'] .
                    $data['is_auth'] .
                    $data['is_capture'] .
                    $data['is_refunded'] .
                    $data['is_standalone_payment'] .
                    $data['is_voided'] .
                    $data['order'] .
                    $data['owner'] .
                    $data['pending'] .
                    $data['source_data_pan'] .
                    $data['source_data_sub_type'] .
                    $data['source_data_type'] .
                    $data['success'];
                break;
            case 'TOKEN':
                $str =
                    $data['card_subtype'] .
                    $data['created_at'] .
                    $data['email'] .
                    $data['id'] .
                    $data['masked_pan'] .
                    $data['merchant_id'] .
                    $data['order_id'] .
                    $data['token'];
                break;
            case 'DELIVERY_STATUS':
                $str =
                    $data['created_at'] .
                    $data['extra_description'] .
                    $data['gps_lat'] .
                    $data['gps_long'] .
                    $data['id'] .
                    $data['merchant'] .
                    $data['order'] .
                    $data['status'];
                break;
        }
        $hash = hash_hmac('sha512', $str, $key);
        return $hash;
    }

}
