<?php
require_once __DIR__ . '/../AbstractRestController.php';

class AcceptModuleFrontController extends AbstractRestController
{
    protected function processGetRequest()
    {
        // do something then output the result
        $this->ajaxDie(json_encode([
            'success' => true,
            'operation' => 'get'
        ]));
    }

    protected function processPostRequest()
    {
        // do something then output the result
        // $this->ajaxDie(json_encode([
        //     'success' => true,
        //     'operation' => 'post'
        // ]));
        $json = json_decode(file_get_contents('php://input'), true);
        $type = $json['type'];
        $data = $json['obj'];
        $obj  = $json['obj'];
        
        if ($type === 'TRANSACTION')
        {
            $data['order']                  = $data['order']['id'];
            $data['is_3d_secure']           = ($data['is_3d_secure'] === true) ? 'true' : 'false';
            $data['is_auth']                = ($data['is_auth'] === true) ? 'true' : 'false';
            $data['is_capture']             = ($data['is_capture'] === true) ? 'true' : 'false';
            $data['is_refunded']            = ($data['is_refunded'] === true) ? 'true' : 'false';
            $data['is_standalone_payment']  = ($data['is_standalone_payment'] === true) ? 'true' : 'false';
            $data['is_voided']              = ($data['is_voided'] === true) ? 'true' : 'false';
            $data['success']                = ($data['success'] === true) ? 'true' : 'false';
            $data['error_occured']          = ($data['error_occured'] === true) ? 'true' : 'false';
            $data['has_parent_transaction'] = ($data['has_parent_transaction'] === true) ? 'true' : 'false';
            $data['pending']                = ($data['pending'] === true) ? 'true' : 'false';
            $data['source_data_pan']        = $data['source_data']['pan'];
            $data['source_data_type']       = $data['source_data']['type'];
            $data['source_data_sub_type']   = $data['source_data']['sub_type'];
            
            //die(dump( $data));
            PrestaShopLogger::addLog("passed data" , 1);
            // print_r($data);
            // die;
            $hash = $this->calculateHash(Configuration::get('credit_cards_hmac'),$data,$type);
           
            // auth?
            if ($hash === Tools::getValue('hmac')) {
                PrestaShopLogger::addLog("passed hmac" , 1);
                
                $order_id = $obj['order']['merchant_order_id'];
                
                if(!$order_id) die("no order with this id");

                if (
                    $obj['success'] === true &&
                    $obj['is_voided'] === false &&
                    $obj['is_refunded'] === false &&
                    $obj['pending'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false &&
                    $obj['error_occured'] === false
                ) {
                    //Payment Sucessfull
                    PrestaShopLogger::addLog("Payment Sucessfull" , 1);
                    $this->updateOrderStatus($order_id, Configuration::get('PS_OS_PAYMENT'));
                    die("Payment Sucessfull");
                    
                } elseif (
                    $obj['success'] === true &&
                    $obj['is_refunded'] === true &&
                    $obj['is_voided'] === false &&
                    $obj['pending'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false
                ) {
                    $this->updateOrderStatus($order_id, Configuration::get('PS_OS_REFUND'));
                    die("Payment Refunded");

                } elseif (
                    $obj['success'] === true &&
                    $obj['is_voided'] === true &&
                    $obj['is_refunded'] === false &&
                    $obj['pending'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false
                ) {
                    //Payment Voided 
                    $this->updateOrderStatus($order_id, Configuration::get('PS_OS_ORDER_CANCELED'));
                    die("Payment Canceled");
                   
                } elseif (
                    $obj['success'] === false &&
                    $obj['is_voided'] === false &&
                    $obj['is_refunded'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false
                ) {
                    //Payment Pending
                    die("Payment Pending");
                }

                       
            } else {
                die('HMAC Failed to validate.');
            }
    
            die('end of update operation');
        }
    

    }

    protected function processPutRequest()
    {
        // do something then output the result
        $this->ajaxDie(json_encode([
            'success' => true,
            'operation' => 'put'
        ]));
    }

    protected function processDeleteRequest()
    {
        // do something then output the result
        $this->ajaxDie(json_encode([
            'success' => true,
            'operation' => 'delete'
        ]));
    }
}