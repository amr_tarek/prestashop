<?php

require_once(dirname(__FILE__).'/AcceptOrderTrait.php');

class AcceptJawwalpayModuleFrontController extends ModuleFrontController
{   
    use AcceptOrderTrait;

    public function initContent()
    {
        parent::initContent();
        //die(dump($this->context->link->getModuleLink('jawwalpay', 'iframe', array(), true)));
        
        //check for redirect url from accept
        if ($this->isResponseCallback()) 
        return $this->redirectUrl();
        
        $proceed = true;
        
        /*
         * If the module is not active anymore, no need to process anything.
         */
        if ($this->module->active == false) {
            die;
        }
        $cart = $this->context->cart;

        $authorized = false;

         if(Context::getContext()->currency->iso_code != Configuration::get('jawwalpay_cards_currency')){
            $this->context->cookie->id_cart = (int) $cart->id;
            Tools::redirect('index.php?controller=order&step=1');
        }
        
          /**
         * Verify if this payment module is authorized
         */
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'accept') {
                $authorized = true;
                break;
            }
        }

        if (!$authorized) {
            die($this->l('This payment method is not available.'));
        }

       
        
        /**
         * Get current cart object from session
         */
       
 
        /**
         * Verify if this module is enabled and if the cart has
         * a valid customer, delivery address and invoice address
         */
         if (!$this->module->active || $cart->id_customer == 0 || $cart->id_address_delivery == 0
            || $cart->id_address_invoice == 0) {
            $this->context->cookie->id_cart = (int) $cart->id;
            Tools::redirect('index.php?controller=order&step=1');
        }

        //die(dump($cart));



        /** @var CustomerCore $customer */
        $customer = new Customer($cart->id_customer);

        /**
         * Check if this is a vlaid customer account
         */
        if (!Validate::isLoadedObject($customer)) {
            $this->context->cookie->id_cart = (int) $cart->id;
            Tools::redirect('index.php?controller=order&step=1');
        
        }

        $cart = $this->context->cart;

        $module_name = $this->module->displayName;
        
        $currency_id = (int) Context::getContext()->currency->id;

        $currencyCode = Context::getContext()->currency->iso_code;

        $amount = (float) $cart->getOrderTotal(true, Cart::BOTH);

        $payment_status = Configuration::get('PS_OS_ACCEPT_PAYMENT');
//        var_dump($cart->orderExists()); die;
// $orderAdded = $this->module->validateOrder((int)$this->context->cart->id,$payment_status,null,$module_name,"pending till getaway confirmation",[],$currency_id,false,$customer->secure_key);
        $orderAdded = $this->module->validateOrder(
        (int) $cart->id,
        $payment_status,
        $amount,
        $module_name,
        "pending till getaway confirmation",
        array(),
        $currency_id, 
        false,
        $customer->secure_key);

        if(!$orderAdded) $proceed = false;
        
        $id_order = Order::getOrderByCartId($cart->id);
        
        if($proceed)
        $token = $this->requestToken(Configuration::get('jawwalpay_cards_api_key'),
        $this->getJawwalPayUrl('api/auth/tokens'));
        
        if(!$token) $proceed = false;
        
        $jawwalOrderId = '';
        
        $items = array(array("name" => 'Total amount - order #'.$cart->id,
                        "amount_cents" => intval($amount)
                    ));
        if($proceed)
        $jawwalOrderId = $this->createOrder($token,
        true,
        Configuration::get('jawwalpay_cards_merchant'),
        $amount,
        $currencyCode,
        $id_order,
        $items,
        [],
        $this->getJawwalPayUrl('api/ecommerce/orders'));

        if(!$jawwalOrderId) $proceed = false;
        
        $billingAndDelivery = $cart->getSummaryDetails();
        
        $delivery = $billingAndDelivery['delivery'];

        $billing = $billingAndDelivery['invoice'];

        $billing_data = $this->parseOrder($delivery,
        $billing,
        $customer->email,
        $billingAndDelivery['delivery_state']);
        
        $iframeKey = '';
        $paymentReturn = null;
        if($proceed)
		$paymentReturn = $this->createPaymentKey($token,
		$amount,
		$jawwalOrderId,
		$billing_data,
		$currencyCode,
		Configuration::get('jawwalpay_cards_int'),
		$this->getJawwalPayUrl('api/acceptance/payment_keys')
		);

          
        if(isset($paymentReturn['token']) )
        {
        	$iframeKey = $paymentReturn['token'];
        }else{
        	$proceed = false;
        }

        if(!$proceed){
                $this->context->cookie->id_cart = (int) $cart->id;
		$this->context->smarty->assign([
			    'message' => $paymentReturn['error'],
			    'src' => '',
			    'success' => false
		]);
		 if (version_compare(_PS_VERSION_,'1.7.0','>')){
		    $this->setTemplate('module:accept/views/templates/front/acceptRedirect2.tpl');
		}else{
		    $this->setTemplate('acceptRedirect.tpl');
		}
        }else{
		$this->context->smarty->assign(array(
		    'src' => $this->getCreditCardIframeUrl($iframeKey)
		));
		if (version_compare(_PS_VERSION_,'1.7.0','>')){
		    $this->setTemplate('module:accept/views/templates/front/acceptIframe2.tpl');
		}else{
		    $this->setTemplate('acceptIframe.tpl');
		}
	}
        
        
    }

    public function postProcess(){
       
        if(!($_SERVER['REQUEST_METHOD'] === 'POST'))return false;

        $json = json_decode(file_get_contents('php://input'), true);
        $type = $json['type'];
        $data = $json['obj'];
        $obj  = $json['obj'];
        
        if ($type === 'TRANSACTION')
        {
            $data['order']                  = $data['order']['id'];
            $data['is_3d_secure']           = ($data['is_3d_secure'] === true) ? 'true' : 'false';
            $data['is_auth']                = ($data['is_auth'] === true) ? 'true' : 'false';
            $data['is_capture']             = ($data['is_capture'] === true) ? 'true' : 'false';
            $data['is_refunded']            = ($data['is_refunded'] === true) ? 'true' : 'false';
            $data['is_standalone_payment']  = ($data['is_standalone_payment'] === true) ? 'true' : 'false';
            $data['is_voided']              = ($data['is_voided'] === true) ? 'true' : 'false';
            $data['success']                = ($data['success'] === true) ? 'true' : 'false';
            $data['error_occured']          = ($data['error_occured'] === true) ? 'true' : 'false';
            $data['has_parent_transaction'] = ($data['has_parent_transaction'] === true) ? 'true' : 'false';
            $data['pending']                = ($data['pending'] === true) ? 'true' : 'false';
            $data['source_data_pan']        = $data['source_data']['pan'];
            $data['source_data_type']       = $data['source_data']['type'];
            $data['source_data_sub_type']   = $data['source_data']['sub_type'];
            
            $hash = $this->calculateHash(Configuration::get('jawwalpay_cards_hmac'),$data,$type);
   
            // auth?
            if ($hash === Tools::getValue('hmac')) {
                
                $order_id = $obj['order']['merchant_order_id'];
                
                if(!$order_id) die("no order with this id");

                if (
                    $obj['success'] === true &&
                    $obj['is_voided'] === false &&
                    $obj['is_refunded'] === false &&
                    $obj['pending'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false &&
                    $obj['error_occured'] === false
                ) {
                    //Payment Sucessfull
                    $this->updateOrderStatus($order_id, Configuration::get('PS_OS_PAYMENT'));
                    die("Payment Sucessfull");
                    
                } elseif (
                    $obj['success'] === true &&
                    $obj['is_refunded'] === true &&
                    $obj['is_voided'] === false &&
                    $obj['pending'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false
                ) {
                    $this->updateOrderStatus($order_id, Configuration::get('PS_OS_REFUND'));
                    die("Payment Refunded");

                } elseif (
                    $obj['success'] === true &&
                    $obj['is_voided'] === true &&
                    $obj['is_refunded'] === false &&
                    $obj['pending'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false
                ) {
                    //Payment Voided 
                    $this->updateOrderStatus($order_id, Configuration::get('PS_OS_ORDER_CANCELED'));
                    die("Payment Canceled");
                   
                } elseif (
                    $obj['success'] === false &&
                    $obj['is_voided'] === false &&
                    $obj['is_refunded'] === false &&
                    $obj['is_void'] === false &&
                    $obj['is_refund'] === false
                ) {
                    //Payment Pending
                    die("Payment Pending");
                }

                       
            } else {
                die('HMAC Failed to validate.');
            }
    
            die('end of update operation');
        }
    }

    public function redirectUrl()
    {   
        $success = false;

        if(Tools::getIsset('order') && Tools::getIsset('data_message') && 
        Tools::getIsset('success') && Tools::getIsset('id') && 
        Tools::getValue('success') && Tools::getValue('data_message') == 'Approved'){
            $objOrder = new Order($_GET["merchant_order_id"]); //order with id=$_GET["action"]
            $history = new OrderHistory();
            $history->id_order = (int)$objOrder->id;
            $history->changeIdOrderState(Configuration::get('PS_OS_PAYMENT'), (int)($objOrder->id)); //order status=4
            $history->addWithemail();
            $history->save();

            $message = "Your payment has been approved";
            $success = true;
        }
        
               
       if($success == false){
            $objOrder = new Order($_GET["merchant_order_id"]); //order with id=$_GET["action"]
           $history = new OrderHistory();
            $history->id_order = (int)$objOrder->id;
            $history->changeIdOrderState(Configuration::get('PS_OS_ERROR'), (int)($objOrder->id)); //order status=4
             $history->save();

            $message = "sorry, your payment isn't approved with error ".Tools::getValue('data_message');
       }
       
        $this->context->smarty->assign([
            'message' => $message,
            'success' => $success
        ]);
        
        if (version_compare(_PS_VERSION_,'1.7.0','>')){
            $this->setTemplate('module:accept/views/templates/front/acceptRedirect2.tpl');
        }else{
            $this->setTemplate('acceptRedirect.tpl');
        }

    }

    private function isResponseCallback()
    {
        if(Tools::getIsset('order') &&
            Tools::getIsset('data_message') && 
            Tools::getIsset('success') &&
            Tools::getIsset('id')
        ) return true; 

        return false;
    }
    

    protected function isValidOrder()
    {
        /*
         * Add your checks right there
         */
        return true;
    }

    private function getJawwalPayUrl($route)
    {
        return Configuration::get('jawwalpay_cards_domain').$route;
    }

    private function getJawwalIframeUrl($token)
    {
        
//      echo   'https://checkoutapi.jawwalpay.ps/api/acceptance/iframes/'.Configuration::get('jawwalpay_cards_iframe').'?payment_token='.$token;
//      echo '<br>';
        return  Configuration::get('jawwalpay_cards_domain')
        .'api/acceptance/iframes/'.
        Configuration::get('jawwalpay_cards_iframe').
        '?payment_token='.$token;
        
//        die;
    }

}
