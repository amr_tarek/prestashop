<?php

class valu{

    public function install()
    {
        return Configuration::updateValue('valu_domain', 'https://accept.paymobsolutions.com');
    }

    public function uninstall()
    {
        if(!Configuration::deleteByName('valu_api_key') ||
        !Configuration::deleteByName('valu_merchant') ||
        !Configuration::deleteByName('valu_hmac') ||
        !Configuration::deleteByName('valu_int') ||
        !Configuration::deleteByName('valu_iframe') ||
        !Configuration::deleteByName('valu_method_name') ||
        !Configuration::deleteByName('valu_iframe_css')||
        !Configuration::deleteByName('valu_domain') ||
        !Configuration::deleteByName('valu_currency')||
        !Configuration::deleteByName('valu_status'))
        {
            return false;
        }

        return true;
    }

    /**
     * Set values for the inputs.
     */
    public function getConfigFormValues()
    {
        return [
            'valu_api_key' => Configuration::get('valu_api_key'),
            'valu_merchant' => Configuration::get('valu_merchant'),
            'valu_hmac' => Configuration::get('valu_hmac'),
            'valu_int' =>  Configuration::get('valu_int'),
            'valu_iframe' =>  Configuration::get('valu_iframe'),
            'valu_method_name' =>  Configuration::get('valu_method_name'),
            'valu_iframe_css' => Configuration::get('valu_iframe_css'),
            'valu_domain' => Configuration::get('valu_domain'),
            'valu_currency' => Configuration::get('valu_currency'),
            'valu_status' => Configuration::get('valu_status'),
        ];

    }
}