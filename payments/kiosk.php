<?php

class kiosk{

    public function install()
    {
        return Configuration::updateValue('kiosk_domain', 'https://accept.paymobsolutions.com');
    }

    public function uninstall()
    {
        if(!Configuration::deleteByName('kiosk_api_key') ||
        !Configuration::deleteByName('kiosk_merchant') ||
        !Configuration::deleteByName('kiosk_hmac') ||
        !Configuration::deleteByName('kiosk_int') ||
        //!Configuration::deleteByName('kiosk_iframe') ||
        !Configuration::deleteByName('kiosk_method_name') ||
        //!Configuration::deleteByName('kiosk_iframe_css')||
        !Configuration::deleteByName('kiosk_domain') ||
        !Configuration::deleteByName('kiosk_currency')||
        !Configuration::deleteByName('kiosk_status'))
        {
            return false;
        }

        return true;
    }

    /**
     * Set values for the inputs.
     */
    public function getConfigFormValues()
    {
        return [
            'kiosk_api_key' => Configuration::get('kiosk_api_key'),
            'kiosk_merchant' => Configuration::get('kiosk_merchant'),
            'kiosk_hmac' => Configuration::get('kiosk_hmac'),
            'kiosk_int' =>  Configuration::get('kiosk_int'),
            //'kiosk_iframe' =>  Configuration::get('kiosk_iframe'),
            'kiosk_method_name' =>  Configuration::get('kiosk_method_name'),
            //'kiosk_iframe_css' => Configuration::get('kiosk_iframe_css'),
            'kiosk_domain' => Configuration::get('kiosk_domain'),
            'kiosk_currency' => Configuration::get('kiosk_currency'),
            'kiosk_status' => Configuration::get('kiosk_status'),
        ];

    }
}