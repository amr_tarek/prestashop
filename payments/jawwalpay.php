<?php

class JawwalPay{

    public function install()
    {
        return Configuration::updateValue('jawwalpay_cards_domain', 'https://checkoutapi.jawwalpay.ps/');
    }

    public function uninstall()
    {
        if(!Configuration::deleteByName('jawwalpay_cards_api_key') ||
        !Configuration::deleteByName('jawwalpay_cards_merchant') ||
        !Configuration::deleteByName('jawwalpay_cards_hmac') ||
        !Configuration::deleteByName('jawwalpay_cards_int') ||
        !Configuration::deleteByName('jawwalpay_cards_iframe') ||
        !Configuration::deleteByName('jawwalpay_cards_method_name') ||
        !Configuration::deleteByName('jawwalpay_cards_iframe_css')||
        !Configuration::deleteByName('jawwalpay_cards_domain') ||
        !Configuration::deleteByName('jawwalpay_cards_currency')||
        !Configuration::deleteByName('jawwalpay_cards_status') 
        )
        {
            return false;
        }

        return true;
    }

    /**
     * Set values for the inputs.
     */
    public function getConfigFormValues()
    {
        return [
            'jawwalpay_cards_api_key' => Configuration::get('jawwalpay_cards_api_key'),
            'jawwalpay_cards_merchant' => Configuration::get('jawwalpay_cards_merchant'),
            'jawwalpay_cards_hmac' => Configuration::get('jawwalpay_cards_hmac'),
            'jawwalpay_cards_int' =>  Configuration::get('jawwalpay_cards_int'),
            'jawwalpay_cards_iframe' =>  Configuration::get('jawwalpay_cards_iframe'),
            'jawwalpay_cards_method_name' =>  Configuration::get('jawwalpay_cards_method_name'),
            'jawwalpay_cards_iframe_css' => Configuration::get('jawwalpay_cards_iframe_css'),
            'jawwalpay_cards_domain' => Configuration::get('jawwalpay_cards_domain'),
            'jawwalpay_cards_currency' => Configuration::get('jawwalpay_cards_currency'),
            'jawwalpay_cards_status' => Configuration::get('jawwalpay_cards_status'),
           
        ];

    }
    
}