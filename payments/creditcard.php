<?php

class CreditCard{

    public function install()
    {
        return Configuration::updateValue('credit_cards_domain', 'https://accept.paymobsolutions.com');
    }

    public function uninstall()
    {
        if(!Configuration::deleteByName('credit_cards_api_key') ||
        !Configuration::deleteByName('credit_cards_merchant') ||
        !Configuration::deleteByName('credit_cards_hmac') ||
        !Configuration::deleteByName('credit_cards_int') ||
        !Configuration::deleteByName('credit_cards_iframe') ||
        !Configuration::deleteByName('credit_cards_method_name') ||
        !Configuration::deleteByName('credit_cards_iframe_css')||
        !Configuration::deleteByName('credit_cards_domain') ||
        !Configuration::deleteByName('credit_cards_currency')||
        !Configuration::deleteByName('credit_cards_status'))
        {
            return false;
        }

        return true;
    }

    /**
     * Set values for the inputs.
     */
    public function getConfigFormValues()
    {
        return [
            'credit_cards_api_key' => Configuration::get('credit_cards_api_key'),
            'credit_cards_merchant' => Configuration::get('credit_cards_merchant'),
            'credit_cards_hmac' => Configuration::get('credit_cards_hmac'),
            'credit_cards_int' =>  Configuration::get('credit_cards_int'),
            'credit_cards_iframe' =>  Configuration::get('credit_cards_iframe'),
            'credit_cards_method_name' =>  Configuration::get('credit_cards_method_name'),
            'credit_cards_iframe_css' => Configuration::get('credit_cards_iframe_css'),
            'credit_cards_domain' => Configuration::get('credit_cards_domain'),
            'credit_cards_currency' => Configuration::get('credit_cards_currency'),
            'credit_cards_status' => Configuration::get('credit_cards_status'),
        ];

    }
}