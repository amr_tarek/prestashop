<?php

class wallet{

    public function install()
    {
        return Configuration::updateValue('wallet_domain', 'https://accept.paymobsolutions.com');
    }

    public function uninstall()
    {
        if(!Configuration::deleteByName('wallet_api_key') ||
        !Configuration::deleteByName('wallet_merchant') ||
        !Configuration::deleteByName('wallet_hmac') ||
        !Configuration::deleteByName('wallet_int') ||
        !Configuration::deleteByName('wallet_iframe') ||
        !Configuration::deleteByName('wallet_method_name') ||
        !Configuration::deleteByName('wallet_iframe_css')||
        !Configuration::deleteByName('wallet_domain') ||
        !Configuration::deleteByName('wallet_currency')||
        !Configuration::deleteByName('wallet_status'))
        {
            return false;
        }

        return true;
    }

    /**
     * Set values for the inputs.
     */
    public function getConfigFormValues()
    {
        return [
            'wallet_api_key' => Configuration::get('wallet_api_key'),
            'wallet_merchant' => Configuration::get('wallet_merchant'),
            'wallet_hmac' => Configuration::get('wallet_hmac'),
            'wallet_int' =>  Configuration::get('wallet_int'),
            'wallet_iframe' =>  Configuration::get('wallet_iframe'),
            'wallet_method_name' =>  Configuration::get('wallet_method_name'),
            'wallet_iframe_css' => Configuration::get('wallet_iframe_css'),
            'wallet_domain' => Configuration::get('wallet_domain'),
            'wallet_currency' => Configuration::get('wallet_currency'),
            'wallet_status' => Configuration::get('wallet_status'),
        ];

    }
}