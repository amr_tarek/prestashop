/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$(document).ready(function(){
    //urls
    var urlJawwalPay = $('#jawwalpayResponseUrl').text();
    var urlCreditCard =  $('#creditCardResponseUrl').text();
    var urlCreditCard =  $('#valuResponseUrl').text();
    var MainDomin = $('#jawwalpay_domain').text();
    var MainDomincc = $('#accept_domain').text();
    var MainDominvalu = $('#accept_domain').text();
    $('.callback_copy').click(function(ev){
      ev.preventDefault();
      var callback = document.createElement("input");
      document.body.appendChild(callback);
      callback.setAttribute("id", "callback_url_accepting");
      document.getElementById("callback_url_accepting").value=urlJawwalPay;
      callback.select();
      document.execCommand("copy");
      document.body.removeChild(callback);
      document.querySelector('.acceptLoader').style.display = 'flex';
      $('.acceptLoader .spinnerac,.acceptLoader .error').hide();
      $('.acceptLoader .success').show();
      $('.acceptLoader .detail').append("Copied <br><span class='button-link' style='color:#03a9f4;'>"+url+"</span><br> to clipboard!<hr>");
      $("html, body").animate({ scrollTop: 0 }, 750);
    });

    $('.callback_copy-cc').click(function(ev){
      ev.preventDefault();
      var callback = document.createElement("input");
      document.body.appendChild(callback);
      callback.setAttribute("id", "callback_cc_url_accepting");
      document.getElementById("callback_cc_url_accepting").value=urlCreditCard;
      callback.select();
      document.execCommand("copy");
      document.body.removeChild(callback);
      document.querySelector('.acceptLoader-cc').style.display = 'flex';
      $('.acceptLoader-cc .spinnerac,.acceptLoader-cc .error').hide();
      $('.acceptLoader-cc .success').show();
      $('.acceptLoader-cc .detail').append("Copied <br><span class='button-link' style='color:#03a9f4;'>"+url+"</span><br> to clipboard!<hr>");
      $("html, body").animate({ scrollTop: 0 }, 750);
    });
    $('.callback_copy-valu').click(function(ev){
      ev.preventDefault();
      var callback = document.createElement("input");
      document.body.appendChild(callback);
      callback.setAttribute("id", "callback_valu_url_accepting");
      document.getElementById("callback_valu_url_accepting").value=urlCreditCard;
      callback.select();
      document.execCommand("copy");
      document.body.removeChild(callback);
      document.querySelector('.acceptLoader-valu').style.display = 'flex';
      $('.acceptLoader-valu .spinnerac,.acceptLoader-valu .error').hide();
      $('.acceptLoader-valu .success').show();
      $('.acceptLoader-valu .detail').append("Copied <br><span class='button-link' style='color:#03a9f4;'>"+url+"</span><br> to clipboard!<hr>");
      $("html, body").animate({ scrollTop: 0 }, 750);
    });


    function _autoLogin(){
      var api_key = $('#accept-api').val();
      if(api_key.length > 0){
        _login();
      }else{
        document.querySelector('.acceptLoader').style.display = 'flex';
        $('.acceptLoader .error,.acceptLoader .success').hide();
        $('.acceptLoader .spinnerac').show();
        $('.acceptLoader .detail').html('Please Enter your "API KEY", then click "Authenticate"<hr>Waiting for key..<hr>');
      }
    }

    function _autoLoginCC(){
      var api_key = $('#accept-cc-api').val();
      if(api_key.length > 0){
        _loginCC();
      }else{
        document.querySelector('.acceptLoader-cc').style.display = 'flex';
        $('.acceptLoader-cc .error,.acceptLoader-cc .success').hide();
        $('.acceptLoader-cc .spinnerac').show();
        $('.acceptLoader-cc .detail').html('Please Enter your "API KEY", then click "Authenticate"<hr>Waiting for key..<hr>');
      }
    }
    function _autoLoginvalu(){
      var api_key = $('#valu-api').val();
      if(api_key.length > 0){
        _loginvalu();
      }else{
        document.querySelector('.acceptLoader-valu').style.display = 'flex';
        $('.acceptLoader-valu .error,.acceptLoader-valu .success').hide();
        $('.acceptLoader-valu .spinnerac').show();
        $('.acceptLoader-valu .detail').html('Please Enter your "API KEY", then click "Authenticate"<hr>Waiting for key..<hr>');
      }
    }

    $('#accept-login').click(function(){_login();});

    $('#accept-cc-login').click(function(){_loginCC();});

    $('#valu-login').click(function(){_loginvalu();});

    _autoLogin();

    _autoLoginCC();

    _autoLoginvalu();

    function _login(){
      var api_key = $('#accept-api').val();
      if(api_key.length == 0){
        document.querySelector('.acceptLoader').style.display = 'flex';
        $('.acceptLoader .error,.acceptLoader .success').hide();
        $('.acceptLoader .spinnerac').show();
        $('.acceptLoader .detail').append('Please Enter your "API KEY"<hr>Waiting for key..<hr>');
      }else{
        document.querySelector('.acceptLoader').style.display = 'flex';
        $('.acceptLoader .error,.acceptLoader .success').hide();
        $('.acceptLoader .spinnerac').show();
        $('.acceptLoader .detail').append("Loading..<hr>");
          var details = {
            "api_key"   : $('#accept-api').val(),
          };
        var requestData = JSON.stringify(details);
        var target  = MainDomin+"/api/auth/tokens";
        $('.acceptLoader .detail').append("Sending API Key to Accept servers..<hr>");
        $.ajax({
          method: "POST",
          contentType : 'application/json',
          url: target,
          data: requestData
        }).done(function(response){
          $('#accept-merchant').val(response.profile.id);
          $('.acceptLoader .success,.acceptLoader .error').hide();
          $('.acceptLoader .spinnerac').show();
          $('.acceptLoader .detail').append("Servers Accepted your API Key<hr>Requesting your list of Integrations now.<hr>");
          _load(response.token);
        }).fail(function(response){
          console.log(response)
          $('.acceptLoader .spinnerac,.acceptLoader .success').hide();
          $('.acceptLoader .error').show();
          $('.acceptLoader .detail').append("Servers Refused your API Key, Please make sure you're using a valid key !<hr>");
        });
      }

    }

    function _loginCC(){
      var api_key = $('#accept-cc-api').val();
      console.log(api_key);
      if(api_key.length == 0){
        document.querySelector('.acceptLoader-cc').style.display = 'flex';
        $('.acceptLoader-cc .error,.acceptLoader-cc .success').hide();
        $('.acceptLoader-cc .spinnerac').show();
        $('.acceptLoader-cc .detail').append('Please Enter your "API KEY"<hr>Waiting for key..<hr>');
      }else{
        document.querySelector('.acceptLoader-cc').style.display = 'flex';
        $('.acceptLoader-cc .error,.acceptLoader-cc .success').hide();
        $('.acceptLoader-cc .spinnerac').show();
        $('.acceptLoader-cc .detail').append("Loading..<hr>");
          var details = {
            "api_key"   : $('#accept-cc-api').val(),
          };
        var requestData = JSON.stringify(details);
        var target  = MainDomincc+"/api/auth/tokens";
        console.log(target);
        $('.acceptLoader-cc .detail').append("Sending API Key to Accept servers..<hr>");
        $.ajax({
          method: "POST",
          contentType : 'application/json',
          url: target,
          data: requestData
        }).done(function(response){
          console.log(response);
          $('#accept-cc-merchant').val(response.profile.id);
          $('.acceptLoader-cc .success,.acceptLoader-cc .error').hide();
          $('.acceptLoader-cc .spinnerac').show();
          $('.acceptLoader-cc .detail').append("Servers Accepted your API Key<hr>Requesting your list of Integrations now.<hr>");
          _loadCC(response.token);
        }).fail(function(response){
          console.log(response)
          $('.acceptLoader-cc .spinnerac,.acceptLoader-cc .success').hide();
          $('.acceptLoader-cc .error').show();
          $('.acceptLoader-cc .detail').append("Servers Refused your API Key, Please make sure you're using a valid key !<hr>");
        });
      }

    }
    function _loginvalu(){
      var api_key = $('#valu-api').val();
      console.log(api_key);
      if(api_key.length == 0){
        document.querySelector('.acceptLoader-valu').style.display = 'flex';
        $('.acceptLoader-valu .error,.acceptLoader-valu .success').hide();
        $('.acceptLoader-valu .spinnerac').show();
        $('.acceptLoader-valu .detail').append('Please Enter your "API KEY"<hr>Waiting for key..<hr>');
      }else{
        document.querySelector('.acceptLoader-valu').style.display = 'flex';
        $('.acceptLoader-valu .error,.acceptLoader-valu .success').hide();
        $('.acceptLoader-valu .spinnerac').show();
        $('.acceptLoader-valu .detail').append("Loading..<hr>");
          var details = {
            "api_key"   : $('#valu-api').val(),
          };
        var requestData = JSON.stringify(details);
        var target  = MainDomincc+"/api/auth/tokens";
        console.log(target);
        $('.acceptLoader-valu .detail').append("Sending API Key to Accept servers..<hr>");
        $.ajax({
          method: "POST",
          contentType : 'application/json',
          url: target,
          data: requestData
        }).done(function(response){
          console.log(response);
          $('#valu-merchant').val(response.profile.id);
          $('.acceptLoader-valu .success,.acceptLoader-valu .error').hide();
          $('.acceptLoader-valu .spinnerac').show();
          $('.acceptLoader-valu .detail').append("Servers Accepted your API Key<hr>Requesting your list of Integrations now.<hr>");
          _loadCC(response.token);
        }).fail(function(response){
          console.log(response)
          $('.acceptLoader-valu .spinnerac,.acceptLoader-valu .success').hide();
          $('.acceptLoader-valu .error').show();
          $('.acceptLoader-valu .detail').append("Servers Refused your API Key, Please make sure you're using a valid key !<hr>");
        });
      }

    }

    function _load(token){
      // get int ids
      $.ajax({
        method: "GET",
        contentType : 'application/json',
        url: MainDomin+"/api/ecommerce/integrations?token="+token,
      }).done(function(response){
        $('#accept-int-list').html("");
        $.each(response.results, function (i, integration) {
          var type = integration.gateway_type;
          if(integration.gateway_type == "VPC"){
            type = "Card";
          }
          if(integration.gateway_type == "CAGG"){
            type = "Aman";
          }
          if(integration.gateway_type == "UIG"){
            type = "Wallet";
          }

          $('#accept-int-list').append($('<option>', {
            value: integration.id,
            text : integration.id+' - '+integration.currency+' - '+type
          }));
        });
        $('.acceptLoader .success,.acceptLoader .error').hide();
        $('.acceptLoader .spinnerac').show();
        $('.acceptLoader .detail').append(
          "Servers sent a list of merchant's Integrations<hr>"
          +"Requesting a list of Iframes now.<hr>"
        );
        // get iframes
        $.ajax({
          method: "GET",
          contentType : 'application/json',
          url: MainDomin+"/api/acceptance/iframes?token="+token,
        }).done(function(response){
          $('#accept-iframe-list').html("");
          $.each(response.results, function (i, iframe) {
            $('#accept-iframe-list').append($('<option>', {
              value: iframe.id,
              text : iframe.id
            }));
          });
          $('.acceptLoader .spinnerac,.acceptLoader .error').hide();
          $('.acceptLoader .success').show();
          $('.acceptLoader .detail').append(
            "Servers sent a list of merchant's Iframes<hr>"
            +"All your data has been loaded successfully!<hr>"
            +"Please Select Your Integration ID and Iframe ID now.<hr>"
            );
        }).fail(function(response){
          $('.acceptLoader .spinnerac,.acceptLoader .success').hide();
          $('.acceptLoader .error').show();
          $('.acceptLoader .detail').append("Unable to get iframe ids.<hr>");
        });
      }).fail(function(response){
        $('.acceptLoader .spinnerac,.acceptLoader .success').hide();
        $('.acceptLoader .error').show();
        $('.acceptLoader .detail').append("Unable to get integration ids.<hr>");
      });
    }

    function _loadCC(token){
      // get int ids
      $.ajax({
        method: "GET",
        contentType : 'application/json',
        url: MainDomincc+"/api/ecommerce/integrations?token="+token,
      }).done(function(response){
        $('#accept-cc-int-list').html("");
        $.each(response.results, function (i, integration) {
          var type = integration.gateway_type;
          if(integration.gateway_type == "VPC"){
            type = "Card";
          }
          if(integration.gateway_type == "CAGG"){
            type = "Aman";
          }
          if(integration.gateway_type == "UIG"){
            type = "Wallet";
          }

          $('#accept-cc-int-list').append($('<option>', {
            value: integration.id,
            text : integration.id+' - '+integration.currency+' - '+type
          }));
        });
        $('.acceptLoader-cc .success,.acceptLoader-cc .error').hide();
        $('.acceptLoader-cc .spinnerac').show();
        $('.acceptLoader-cc .detail').append(
          "Servers sent a list of merchant's Integrations<hr>"
          +"Requesting a list of Iframes now.<hr>"
        );
        // get iframes
        $.ajax({
          method: "GET",
          contentType : 'application/json',
          url: MainDomincc+"/api/acceptance/iframes?token="+token,
        }).done(function(response){
          $('#accept-cc-iframe-list').html("");
          $.each(response.results, function (i, iframe) {
            console.log("load",response);
            $('#accept-cc-iframe-list').append($('<option>', {
              value: iframe.id,
              text : iframe.id
            }));
          });
          $('.acceptLoader-cc .spinnerac,.acceptLoader-cc .error').hide();
          $('.acceptLoader-cc .success').show();
          $('.acceptLoader-cc .detail').append(
            "Servers sent a list of merchant's Iframes<hr>"
            +"All your data has been loaded successfully!<hr>"
            +"Please Select Your Integration ID and Iframe ID now.<hr>"
            );
        }).fail(function(response){
          $('.acceptLoader-cc .spinnerac,.acceptLoader-cc .success').hide();
          $('.acceptLoader-cc .error').show();
          $('.acceptLoader-cc .detail').append("Unable to get iframe ids.<hr>");
        });
      }).fail(function(response){
        $('.acceptLoader-cc .spinnerac,.acceptLoader-cc .success').hide();
        $('.acceptLoader-cc .error').show();
        $('.acceptLoader-cc .detail').append("Unable to get integration ids.<hr>");
      });
    }
    function _loadvalu(token){
      // get int ids
      $.ajax({
        method: "GET",
        contentType : 'application/json',
        url: MainDomincc+"/api/ecommerce/integrations?token="+token,
      }).done(function(response){
        $('#valu-int-list').html("");
        $.each(response.results, function (i, integration) {
          var type = integration.gateway_type;
          if(integration.gateway_type == "VPC"){
            type = "Card";
          }
          if(integration.gateway_type == "CAGG"){
            type = "Aman";
          }
          if(integration.gateway_type == "UIG"){
            type = "Wallet";
          }
          if(integration.gateway_type == "VALU"){
            type = "valU";
          }

          $('#valu-int-list').append($('<option>', {
            value: integration.id,
            text : integration.id+' - '+integration.currency+' - '+type
          }));
        });
        $('.acceptLoader-valu .success,.acceptLoader-valu .error').hide();
        $('.acceptLoader-valu .spinnerac').show();
        $('.acceptLoader-valu .detail').append(
          "Servers sent a list of merchant's Integrations<hr>"
          +"Requesting a list of Iframes now.<hr>"
        );
        // get iframes
        $.ajax({
          method: "GET",
          contentType : 'application/json',
          url: MainDomincc+"/api/acceptance/iframes?token="+token,
        }).done(function(response){
          $('#valu-iframe-list').html("");
          $.each(response.results, function (i, iframe) {
            console.log("load",response);
            $('#valu-iframe-list').append($('<option>', {
              value: iframe.id,
              text : iframe.id
            }));
          });
          $('.acceptLoader-valu .spinnerac,.acceptLoader-valu .error').hide();
          $('.acceptLoader-valu .success').show();
          $('.acceptLoader-valu .detail').append(
            "Servers sent a list of merchant's Iframes<hr>"
            +"All your data has been loaded successfully!<hr>"
            +"Please Select Your Integration ID and Iframe ID now.<hr>"
            );
        }).fail(function(response){
          $('.acceptLoader-valu .spinnerac,.acceptLoader-valu .success').hide();
          $('.acceptLoader-valu .error').show();
          $('.acceptLoader-valu .detail').append("Unable to get iframe ids.<hr>");
        });
      }).fail(function(response){
        $('.acceptLoader-valu .spinnerac,.acceptLoader-valu .success').hide();
        $('.acceptLoader-valu .error').show();
        $('.acceptLoader-valu .detail').append("Unable to get integration ids.<hr>");
      });
    }

    var getCurrency = function(selector){
      var listText = $(selector+' option:selected').text();
      var currency = listText.split(' - ')[1];
      return currency;
    }

    $('#accept-int').on('change',function(){
        var currency = getCurrency('#accept-int');
        $('#jawwalpay_cards_currency').val(currency);
    })
    $('#accept-cc-int').on('change',function(){
        var currency = getCurrency('#accept-cc-int');
        $('#credit_cards_currency').val(currency);
    })
    $('#valu-int').on('change',function(){
      var currency = getCurrency('#valu-int');
      $('#valu_currency').val(currency);
  })
  });
