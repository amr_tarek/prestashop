{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
	<li  {if ($redirectTo == 'jawwalpay')} class="active" {/if} ><a href="#template_1" role="tab" data-toggle="tab">Jawwal Pay</a></li>
	<li  {if ($redirectTo == 'kiosk')} class="active" {/if} ><a href="#template_kiosk" role="tab" data-toggle="tab">Accept-Kiosk</a></li>
	<li  {if ($redirectTo == 'valu')} class="active" {/if} ><a href="#template_valu" role="tab" data-toggle="tab">Accept-valU</a></li>
	<li  {if ($redirectTo == 'creditcard')} class="active" {/if} ><a href="#template_2" role="tab" data-toggle="tab">Accept-Credit Card</a></li>
	<li  {if ($redirectTo == 'kiosk')} class="active" {/if} ><a href="#template_wallet" role="tab" data-toggle="tab">Accept-Wallet</a></li>

</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div class="tab-pane  {if ($redirectTo == 'jawwalpay')} active {/if} " id="template_1">{include file='./template_1.tpl'}</div>
	<div class="tab-pane  {if ($redirectTo == 'kiosk')} active {/if} " id="template_kiosk">{include file='./template_kiosk.tpl'}</div>
	<div class="tab-pane  {if ($redirectTo == 'valu')} active {/if} " id="template_valu">{include file='./template_valu.tpl'}</div>
	<div class="tab-pane {if ($redirectTo == 'creditcard')} active {/if} " id="template_2">{include file='./template_2.tpl'}</div>
	<div class="tab-pane  {if ($redirectTo == 'wallet')} active {/if} " id="template_wallet">{include file='./template_wallet.tpl'}</div>

</div>
