<div class="panel">
    <div class="panel-heading">
        <i class="fa fa-pencil"></i> Editing wallet
    </div>
    <div class="panel-body">
      <div style="display:none;" id="accept_domain">{$wallet_domain}</div>
	   <div style="display:none;" id="walletResponseUrl">{$walletResponseUrl}</div>
      <form action="{$saveUrl}" method="post" enctype="multipart/form-data" id="form-payment" class="form-horizontal">
        <div class="tab-content">
          	<input type="hidden" name="wallet_currency" id="wallet_currency" value="{$wallet_currency}" />
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="">Payment Gateway Status</label>
            <div class="col-sm-10">
              <select name="wallet_status" id="wallet_status" class="form-control">
                {if ($wallet_status)}
                  <option value="0">Disabled</option>
                <option value="1" selected="selected">Enabled</option>

                {else}
                  <option value="0" selected="selected">Disabled</option>
                <option value="1" >Enabled</option>

                {/if}
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
            
              <div class="acceptLoader-wallet .achide">
                <span class="spinnerac default"></span>
                <span class="success achide"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"> <path fill="none" d="M0 0h24v24H0V0zm0 0h24v24H0V0z"/> <path d="M16.59 7.58L10 14.17l-3.59-3.58L5 12l5 5 8-8zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/> </svg></span>
                <span class="error achide"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px"height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve"> <g id="Bounding_Boxes"> <path opacity="0.87" fill="none" d="M0,0h24v24H0V0z"/> </g> <g id="Outline_1_"> <g> <path d="M12,2C6.47,2,2,6.47,2,12c0,5.53,4.47,10,10,10c5.53,0,10-4.47,10-10C22,6.47,17.53,2,12,2z M12,20c-4.41,0-8-3.59-8-8 s3.59-8,8-8s8,3.59,8,8S16.41,20,12,20z"/> <polygon points="15.59,7 12,10.59 8.41,7 7,8.41 10.59,12 7,15.59 8.41,17 12,13.41 15.59,17 17,15.59 13.41,12 17,8.41     "/> </g> </g> </svg></span>
                <span class="detail"></span>
              </div>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="wallet-api">API KEY</label>
            <div class="col-sm-10">
              <input type="text" id="wallet-api" name="wallet_api_key" placeholder="Enter your Accept API KEY." value="{$wallet_api_key}" class="form-control"/>
            </div>
          </div>
          <div class="form-group text-center">
            <div class="col-sm-12">
              <p><span id="wallet-login" class="btn btn-primary btn-lg" style="cursor: pointer;">Authenticate</span></p>
              <p>Don't have an account? <a href='https://weaccept.co/portal/register' target='_blank'>Sign up here</a></p>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="wallet-merchant">Merchant ID</label>
            <div class="col-sm-10">
              <input type="text" id="wallet-merchant" name="wallet_merchant" placeholder="Enter your merchant id." value="{$wallet_merchant}" class="form-control"/>
              <span class="help-block">Inset your MERCHANT ID Secret can be found <a href="https://weaccept.co/portal/settings" target="_blank">here</a>.</span>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="wallet-hmac">HMAC Secret</label>
            <div class="col-sm-10">
              <input type="text" id="wallet-hmac" name="wallet_hmac" placeholder="Enter your Accept hmac." value="{$wallet_hmac}" class="form-control"/>
              <span class="help-block">Inset your HMAC SECRET can be found <a href="https://weaccept.co/portal/settings" target="_blank">here</a>.</span>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="wallet-int">Integration ID</label>
            <div class="col-sm-10">
              <select name="wallet_int" id="wallet-int" class="form-control">
                {if ($wallet_int)}
                  <option value="{$wallet_int}" selected="selected">{$wallet_int}</option>
                {else}
                  <option disabled="disabled" selected="selected"></option>
                {/if}
                <optgroup label="Available Integration IDs." id="wallet-int-list">
                  <option disabled="disabled">Nothing.</option>
                </optgroup>
              </select>
              <span class="help-block">Please login first and your list of Integrations will be created automatically.</span>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="wallet-iframe">Iframe ID</label>
            <div class="col-sm-10">
              <select name="wallet_iframe" id="wallet-iframe" class="form-control">
                {if ($wallet_iframe)}
                <option value="{$wallet_iframe}" selected="selected">{$wallet_iframe}</option>
                {else}
                  <option disabled="disabled" selected="selected"></option>
                {/if}
                <optgroup label="Available Iframe IDs." id="wallet-iframe-list">
                  <option disabled="disabled">Nothing.</option>
                </optgroup>
              </select>
              <span class="help-block">Please login first and your list of Iframes will be created automatically.</span>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="wallet-method-name">Method name</label>
            <div class="col-sm-10">
              <input type="text" id="wallet-method-name" name="wallet_method_name" placeholder="Enter method name that will be shown at checkout." value="{$wallet_method_name}" class="form-control"/>
              <span class="help-block">This Controls what this method name will be displayed at the checkout. example Credit Cards, wallet, Aman, Phone Wallets.</span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="wallet-iframe-css">iFrame Style (css)</label>
            <div class="col-sm-10">
              {if ($wallet_iframe_css)}
              <input type="text" id="wallet-iframe-css" name="wallet_iframe_css" placeholder="Enter css to be used on the iframe." value="{$wallet_iframe_css}" class="form-control"/>
              {else}
                <input type="text" id="wallet-iframe-css" name="wallet_iframe_css" placeholder="Enter css to be used on the iframe." value="width: 99%;margin: 0 auto;min-height: 97vh;border: none;" class="form-control"/>
              {/if}
              <span class="help-block">Here you may Add CSS styling to your iframe directly (Inline style) for more information about css styling please follow this <a href="https://www.w3schools.com/css/css_howto.asp" target="_blank">link</a>.<br><b>Default style is</b>: <code>width: 99%;margin: 0 auto;min-height: 97vh;border: none;</code></span>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">Callback URL</label>
            <div class="col-sm-10">
              <span class="callback_copy-wallet btn btn-info" id="callback_help" aria-describedby="callback_help">Copy Link</span>
              <span id="callback_help" class="help-block">Use this link as Transaction processed &amp;Transaction response callback in your <a href="https://weaccept.co/portal/integrations" target="_blank">Payment Integrations</a>.</span>
            </div>
          </div>
        </div>

        <div class="form-group">
             <button type="submit" class="btn btn-primary btn-lg pull-right" name="submit_wallet" >Save</button>
        </div>

      </form>
    </div>
</div>