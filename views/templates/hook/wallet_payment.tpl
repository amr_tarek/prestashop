{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<p class="payment_module" id="wallet_payment_button">
		<a  class="bankwire" href="{$link->getModuleLink('accept', 'wallet', array(), true)|escape:'htmlall':'UTF-8'}" title="{l s='Pay with Accept - PayMob module' mod='wallet'}">
			{l s='Pay with Accept - PayMob module' mod='wallet'}
		</a>
		

</p>
if(isset($paymentReturn['token']) )
        {
        	$iframeKey = $paymentReturn['token'];
        }else{
        	$proceed = false;
        }
        //$wphone=$this-> walletphonenum();
        $phone_number = $_POST['wphone'];
        // print_r($phone_number);
        // die;
        $redirectURL = '';
        if($proceed)
        $redirectURL = $this->requestWalletUrl($phone_number,$iframeKey,$token
        );
        if(!$proceed){
        $this->context->cookie->id_cart = (int) $cart->id;
        $this->context->smarty->assign([
                'message' => $paymentReturn['error'],
                'src' => '',
                'success' => false
        ]);
        $this->setTemplate('module:accept/views/templates/front/acceptRedirect2.tpl');
        }else{
        Tools::redirect($redirectURL);
        //$this->setTemplate('module:accept/views/templates/front/kioskRedirect.tpl');
        
        } 

