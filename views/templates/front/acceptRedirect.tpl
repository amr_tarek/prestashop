<section>
<div>
    {if ($success)}
        <div class="alert alert-success">
            {$message}
        <div>
    {else}
        <div class="alert alert-warning">
            {$message}
        </div>
    {/if}
</div>
</section>
