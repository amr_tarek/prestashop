{extends "$layout"}
{block name="content"}
    <section>
        <div>
            {if ($success)}
                <div class="alert alert-success">
                    <h4>Your bill reference is</h4> <h1 style="color:red">{$message}</h1><h4>Please head to the nearest Aman outlet, ask for "Madfouaat Motanouea Accept" and provide your bill reference.</h4>
                </div>
            {else}
                <div class="alert alert-warning">
                    {$message}
                </div>
            {/if}
        </div>
    </section>
{/block}
