<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__).'/payments/creditcard.php');
require_once(dirname(__FILE__).'/payments/jawwalpay.php');
require_once(dirname(__FILE__).'/payments/valu.php');
require_once(dirname(__FILE__).'/payments/kiosk.php');
require_once(dirname(__FILE__).'/payments/wallet.php');



class Accept extends PaymentModule
{
    protected $config_form = false;


    private $creditCard;

    private $jawwalPay;

    private $valu;

    private $kiosk;

    private $wallet;

    private $allowedMethods = ['jawwalpay','creditcard','valu','kiosk','wallet'];

    public function __construct()
    {
        $this->name = 'accept';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'Abdalla zaki';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Accept Payments');
        $this->description = $this->l('Accept is a payments gataways');

        $this->confirmUninstall = $this->l('');

        $this->limited_countries = array();

        $this->limited_currencies = array();

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

        $this->creditCard = new CreditCard();

        $this->jawwalPay = new JawwalPay();

        $this->valu = new valu();

        $this->kiosk = new kiosk();

        $this->wallet = new wallet();
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (extension_loaded('curl') == false)
        {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }

        // $iso_code = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));

        // if (in_array($iso_code, $this->limited_countries) == false)
        // {
        //     $this->_errors[] = $this->l('This module is not available in your country');
        //     return false;
        // }
	// create new order status STATUSNAME

	// $values_to_insert = array(

	// 'invoice' => 0,

	// 'send_email' => 0,

	// 'module_name' => $this->name,

	// 'color' => 'RoyalBlue',

	// 'unremovable' => 0,

	// 'hidden' => 0,

	// 'logable' => 0,

	// 'delivery' => 0,

	// 'shipped' => 0,

	// 'paid' => 0,

	// 'deleted' => 0);

	 

	// if(!Db::getInstance()->execute(_DB_PREFIX_.'order_state', $values_to_insert, 'INSERT')){
    //     return true;
    // }else{
    //     return true;
    // }

	

	// $id_order_state = (int)Db::getInstance()->Insert_ID();

	// $languages = Language::getLanguages(false);

	// foreach ($languages as $language)

    // Db::getInstance()->execute(_DB_PREFIX_.'order_state_lang', array('id_order_state'=>$id_order_state, 'id_lang'=>$language['id_lang'], 'name'=>'Awaiting accept Payment', 'template'=>''), 'INSERT');

	// if (!@copy(dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.gif', _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'os'.DIRECTORY_SEPARATOR.$id_order_state.'.gif'))



	// Configuration::updateValue('PS_OS_ACCEPT_PAYMENT', $id_order_state);

	// unset($id_order_state);
        Configuration::updateValue('ACCEPT_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('payment') &&
            $this->registerHook('paymentReturn') &&
            $this->registerHook('paymentOptions') &&
            $this->registerHook('displayPayment') &&
            $this->creditCard->install() &&
            $this->valu->install() &&
            $this->kiosk->install() &&
            $this->wallet->install() &&  
            $this->jawwalPay->install();
    
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
        !Configuration::deleteByName('MYMODULE_NAME') ||
        !Configuration::deleteByName('ACCEPT_LIVE_MODE')||
        !$this->creditCard->uninstall()||
        !$this->valu->uninstall()||
        !$this->kiosk->uninstall()||
        !$this->wallet->uninstall()||
        !$this->jawwalPay->uninstall())
        {
            return false;
        }

        return true;
    }

/**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        $output = null;
        $redirectTo = 'kiosk';
        $submitType = $this->checkForPaymentType(Tools::getAllValues());
    
        if ($submitType  && Tools::isSubmit('submit_'.$submitType) ){
            
            $selectedMethod = str_replace('submit_','',$submitType);

            $redirectTo = $selectedMethod;

            if ($this->validateConfigSettings($this->getConfigFormValues($selectedMethod))){
                $this->postProcess( $selectedMethod);
               
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }else{
                $output .= $this->displayError($this->l('Invalid Configuration Setting'));
            }
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        //$this->renderForm();

        $saveUrl = AdminController::$currentIndex.
        '&configure='.$this->name.'&save'.$this->name.
        '&token='.Tools::getAdminTokenLite('AdminModules');
        $controllerData = [
            'saveUrl' => $saveUrl,
            'redirectTo' => $redirectTo,
            'creditCardResponseUrl' => $this->context->link->getModuleLink($this->name, 'creditcard', array(), true),
            'jawwalpayResponseUrl'=>  $this->context->link->getModuleLink($this->name, 'jawwalpay', array(), true),
            'valuResponseUrl' => $this->context->link->getModuleLink($this->name, 'valu', array(), true),
            'kioskResponseUrl' => $this->context->link->getModuleLink($this->name, 'kiosk', array(), true),
            'walletResponseUrl' => $this->context->link->getModuleLink($this->name, 'wallet', array(), true),

        ];
        $this->context->smarty->assign(array_merge(
            $controllerData,
            $this->getConfigFormValuesAll()
        ));

        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {   

        $saveUrl = AdminController::$currentIndex.
        '&configure='.$this->name.'&save'.$this->name.
        '&token='.Tools::getAdminTokenLite('AdminModules');
        $controllerData = ['saveUrl' => $saveUrl];
        $this->context->smarty->assign(array_merge(
            $controllerData,
            $this->getConfigFormValuesAll()
        ));
    }
    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues($selectedMethod)
    {
        switch ($selectedMethod) {
            case 'creditcard':
               return $this->creditCard->getConfigFormValues();
               break;
            case 'jawwalpay':
               return $this->jawwalPay->getConfigFormValues();
               break;
            case 'valu':
                return $this->valu->getConfigFormValues();
                break;
            case 'kiosk':
                return $this->kiosk->getConfigFormValues();
                break;
            case 'wallet':
                return $this->wallet->getConfigFormValues();
                break;            
           default:
               # code...
               break;
        }

    }

    public function getConfigFormValuesAll()
    {
        return array_merge($this->creditCard->getConfigFormValues(),
        $this->jawwalPay->getConfigFormValues(),
        $this->valu->getConfigFormValues(),
        $this->wallet->getConfigFormValues(),
        $this->kiosk->getConfigFormValues());
    }

    private function validateConfigSettings($inputs){
        
        unset($inputs['jawwalpay_cards_domain']);
        unset($inputs['credit_cards_domain']);
        unset($inputs['valu_domain']);
        unset($inputs['kiosk_domain']);
        unset($inputs['wallet_domain']);
        unset($inputs['jawwalpay_cards_status']);
        unset($inputs['credit_cards_status']);
        unset($inputs['valu_status']);
        unset($inputs['kiosk_status']);
        unset($inputs['wallet_status']);
        
         foreach (array_keys($inputs) as $input){
            $inputValue = Tools::getValue($input);
            if(empty($inputValue)) return false;
            //!Validate::isGenericName($inputValue)) 
            // if(empty($inputValue))
            // $array[$input] = [false,$inputValue]; 
            // else
            // $array[$input] = [true,$inputValue];  
        }

        return true;

    }

    /**
     * Save form data.
     */
    protected function postProcess($selectedMethod)
    {
        $form_values = $this->getConfigFormValues($selectedMethod);

        unset($form_values['jawwalpay_cards_domain']);

        unset($form_values['credit_cards_domain']);
        unset($form_values['valu_domain']);
        unset($form_values['kiosk_domain']);
        unset($form_values['wallet_domain']);


        
        foreach (array_keys($form_values) as $key) {

            Configuration::updateValue($key, Tools::getValue($key));
           
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {   
        
        if (isset($_GET['configure']) && $_GET['configure'] == $this->name){
            $this->context->controller->addJquery();

            $this->context->controller->addJS($this->_path.'views/js/back_accept.js');
            $this->context->controller->addCSS($this->_path.'views/css/back_accept.css');

        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front_accept.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front_accept.css');
    }

    /**
     * Return payment options available for PS 1.7+
     *
     * @param array Hook parameters
     *
     * @return array|null
     */
    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        // if (!$this->checkCurrency($params['cart'])) {
        //     return;
        // }
        $paymentOptions = [];

        if(Configuration::get('credit_cards_status'))
        $paymentOptions[] = $this->getCreditCardPaymentOption();
        if(Configuration::get('jawwalpay_cards_status'))
        $paymentOptions[] = $this->getJawwalPayPaymentOption();
        if(Configuration::get('valu_status'))
        $paymentOptions[] = $this->getvaluPaymentOption();
        if(Configuration::get('kiosk_status'))
        $paymentOptions[] = $this->getkioskPaymentOption();
        if(Configuration::get('wallet_status'))
        $paymentOptions[] = $this->getwalletPaymentOption();

        return $paymentOptions;

    }

    public function getCreditCardPaymentOption()
    {
        $iframeOption = new \PrestaShop\PrestaShop\Core\Payment\PaymentOption();
        $iframeOption->setCallToActionText($this->l('Accept-Credit Card'))
                     ->setAction($this->context->link->getModuleLink($this->name, 'creditcard', array(), true));
                     //->setAdditionalInformation($this->context->smarty->fetch('module:accept/views/templates/front/card_infos.tpl'));

                     //->setAdditionalInformation($this->context->smarty->fetch('module:accept/views/templates/front/card_infos.tpl'));
                    //  ->setLogo(Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/payment.jpg'));

        return $iframeOption;
    }
    public function getvaluPaymentOption()
    {
        $iframeOption = new \PrestaShop\PrestaShop\Core\Payment\PaymentOption();
        $iframeOption->setCallToActionText($this->l('valU'))
                     ->setAction($this->context->link->getModuleLink($this->name, 'valu', array(), true));
                     //->setAdditionalInformation($this->context->smarty->fetch('module:paymentexample/views/templates/front/payment_infos.tpl'))
                     //->setLogo(Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/payment.jpg'));

        return $iframeOption;
    }
    public function getkioskPaymentOption()
    {
        $iframeOption = new \PrestaShop\PrestaShop\Core\Payment\PaymentOption();
        $iframeOption->setCallToActionText($this->l('Accept-Kiosk'))
                     ->setAction($this->context->link->getModuleLink($this->name, 'kiosk', array(), true));
                     //->setAdditionalInformation($this->context->smarty->fetch('module:paymentexample/views/templates/front/payment_infos.tpl'))
                     //->setLogo(Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/payment.jpg'));

        return $iframeOption;
    }
    public function getwalletPaymentOption()
    {
        $iframeOption = new \PrestaShop\PrestaShop\Core\Payment\PaymentOption();
        //$paymentForm = $this->fetch('module:accept/views/templates/hook/wallet_form.tpl');
        $iframeOption->setCallToActionText($this->l('Accept-Wallet'))
                     //->setForm($paymentForm)
                     ->setAction($this->context->link->getModuleLink($this->name, 'wallet', array(), true));
                     //->setAdditionalInformation($this->context->smarty->fetch('module:accept/views/templates/front/wallet_infos.tpl'));
                     //->setAdditionalInformation($this->context->smarty->fetch('modules:accept/views/templates/front/wallet_infos.tpl'));
                     //->setLogo(Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/payment.jpg'));

        return $iframeOption;
    }

    public function getJawwalPayPaymentOption()
    {
        $iframeOption = new \PrestaShop\PrestaShop\Core\Payment\PaymentOption();
        $iframeOption->setCallToActionText($this->l('Jawwal Pay'))
                     ->setAction($this->context->link->getModuleLink($this->name, 'jawwalpay', array(), true));
                     //->setAdditionalInformation($this->context->smarty->fetch('module:paymentexample/views/templates/front/payment_infos.tpl'))
                     //->setLogo(Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/payment.jpg'));

        return $iframeOption;
    }

    public function checkCurrency($cart)
    {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);
        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }

    public function checkForPaymentType($inputs)
    {
        
        foreach (array_keys($inputs) as $input) {
           $submit = str_replace('submit_','',$input);

           if(in_array($submit,$this->allowedMethods)) return  $submit;
        }

       return false;
    }
    public function hookPayment($params)
    {
        $currency_id = $params['cart']->id_currency;
        $currency = new Currency((int)$currency_id);

        if (in_array($currency->iso_code, $this->limited_currencies) == false)
            //return false;

        $this->smarty->assign('module_dir', $this->_path);
        if(Configuration::get('credit_cards_status'))
            return $this->display(__FILE__, 'views/templates/hook/cc_payment.tpl');
        if(Configuration::get('jawwalpay_cards_status'))
            return $this->display(__FILE__, 'views/templates/hook/jawwal_payment.tpl');
        if(Configuration::get('valu_status'))
            return $this->display(__FILE__, 'views/templates/hook/valu_payment.tpl');
        if(Configuration::get('kiosk_status'))
            return $this->display(__FILE__, 'views/templates/hook/kiosk_payment.tpl');
        if(Configuration::get('wallet_status'))
            return $this->display(__FILE__, 'views/templates/hook/wallet_payment.tpl');    
    }
    // public function hookActionPaymentConfirmation()
    // {
    //     /* Place your code here. */
    // }

    // public function hookDisplayPayment()
    // {
    //     /* Place your code here. */
    // }

    // public function hookDisplayPaymentReturn()
    // {
    //     /* Place your code here. */
    // }
}
